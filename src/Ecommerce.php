<?php

namespace DWP\Medoro;

ini_set("soap.wsdl_cache_enabled", "0");

/**
 * Class Ecommerce
 * @package DWPMedoro
 */
class Ecommerce {

    public $config;

    private $_merchantKey	= null;
    private $_systemKey 	= null;

    /**
     * Ecommerce constructor.
     *
     * @param array $config
     */
    public function __construct($config = array())
    {
        $this->config = $config;
        $this->loadKeys();
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->exec($name, $arguments[0]);
    }

    /**
     * @param $array_data
     * @param array $additional_fields
     * @return \StdClass
     * @throws \Exception
     */
    public function prepare($array_data, $additional_fields=[])
    {
        $clean_data = $this->arrayToXml(new \SimpleXMLElement('<data />'), $array_data)->asXML();
        $sign       = $this->sign($clean_data);
        $tmp        = $this->encrypt($clean_data);

        // Base64 encode everything
        $sign = base64_encode($sign);
        $key  = base64_encode($tmp['key']);
        $data = base64_encode($tmp['data']);

        // Form request
        $request = new \StdClass();
        $request->INTERFACE = $this->config['merchant_id'];
        $request->KEY_INDEX = $this->config['key_index'];
        $request->KEY = $key;
        $request->DATA = $data;
        $request->SIGNATURE = $sign;

        return $request;

    }

    /**
     * @param $response
     * @return \SimpleXMLElement
     * @throws \Exception
     */
    public function parse($response)
    {
        $sign = base64_decode($response->SIGNATURE);
        $key  = base64_decode($response->KEY);
        $data = base64_decode($response->DATA);

        $data = $this->decrypt($data, $key);

        if (!$this->checkSignature($data, $sign)) {
            throw new \Exception('Decryption failed, invalid signature!');
        }

        return simplexml_load_string($data);

    }

    /**
     * @param $clean_data
     * @return mixed
     * @throws EcommerceException
     */
    private function sign($clean_data)
    {
        $merchantKeyId = openssl_get_privatekey($this->_merchantKey);
        if (!openssl_sign($clean_data, $sign, $merchantKeyId)) {
            $errorMessage = '';
            while ($message = openssl_error_string()) {
                $errorMessage .= $message ."\n";
            }
            throw new EcommerceException("Signing failed.", [
                'errorMessage'  => $errorMessage,
                'config'        => $this->config,
                'cleanData'     => $clean_data,
                'merchantKeyId' => $merchantKeyId,
                'signature'     => $sign,
                'merchantKey'   => $this->_merchantKey,
            ]);
        }
        openssl_free_key($merchantKeyId);

        return $sign;
    }

    /**
     * @param $data
     * @param $sign
     * @return bool
     */
    private function checkSignature($data, $sign)
    {
        $systemKeyId = openssl_get_publickey($this->_systemKey);
        $res = (openssl_verify($data, $sign, $systemKeyId) == 1);
        openssl_free_key($systemKeyId);

        return $res;
    }

    /**
     * @param $clearData
     * @return array
     * @throws EcommerceException
     */
    private function encrypt($clearData)
    {
        $systemKeyId = openssl_get_publickey($this->_systemKey);
        if (openssl_seal($clearData, $data, $eKeys, array($systemKeyId))) {
            $key = $eKeys[0];
        } else {
            throw new EcommerceException("Encryption failed.", [
                'errorMessage'  => openssl_error_string(),
                'config'        => $this->config,
            ]);
        }
        openssl_free_key($systemKeyId);

        return array(
            'data' => $data,
            'key' => $key,
        );
    }

    /**
     * @param $data
     * @param $key
     * @return mixed
     * @throws EcommerceException
     */
    private function decrypt($data, $key)
    {
        $merchantKeyId = openssl_get_privatekey($this->_merchantKey);
        if (!openssl_open($data, $cleardata, $key, $merchantKeyId)) {
            throw new EcommerceException("Decryption failed.", [
                'errorMessage'  => openssl_error_string(),
                'config'        => $this->config,
            ]);
        };
        openssl_free_key($merchantKeyId);

        return $cleardata;
    }

    /**
     *
     */
    public function loadKeys()
    {
        $this->_merchantKey = file_get_contents($this->config['merchant_key']);
        $this->_systemKey = file_get_contents($this->config['gateway_key']);
    }

    public function dumpKeys()
    {
        echo "Merchant Key:\n";
        var_dump($this->config['merchant_key']);
        var_dump(filesize($this->config['merchant_key']));
        var_dump($this->_merchantKey);

        echo "Gateway Key:\n";
        var_dump($this->config['gateway_key']);
        var_dump(filesize($this->config['gateway_key']));
        var_dump($this->_systemKey);
    }

    /**
     * @param \SimpleXMLElement $xml
     * @param array $children
     * @return \SimpleXMLElement
     */
    private function arrayToXml(\SimpleXMLElement $xml, array $children)
    {
        foreach ($children as $name => $value) {
            (is_array($value)) ? $this->arrayToXml($xml->addChild($name), $value) : $xml->addChild($name, $value);
        }

        return $xml;
    }
}