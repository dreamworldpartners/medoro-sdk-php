<?php

namespace DWP\Medoro;

/**
 * Class EcommerceException
 * @package DWPMedoro
 */
class EcommerceException extends \Exception
{
    public $config;
    public $errorMessage;
    public $cleanData;
    public $merchantKeyId;
    public $signature;
    public $merchantKey;

    /**
     * EcommerceException constructor.
     * @param string $message
     * @param array $extra
     */
    public function __construct($message = "", $extra=[])
    {
        parent::__construct($message, 0, null);

        $this->config       = json_encode($extra['config'] ?? null);
        $this->errorMessage = json_encode($extra['errorMessage'] ?? null);
        $this->cleanData    = $extra['cleanData'] ?? null;
        $this->signature    = $extra['signature'] ?? null;
        $this->merchantKey  = $extra['merchantKey'] ?? null;
    }
}