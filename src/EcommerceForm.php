<?php

namespace DWP\Medoro;

ini_set("soap.wsdl_cache_enabled", "0");

class EcommerceForm
{
    private static $required = ['interface', 'KEY_INDEX', 'KEY', 'DATA', 'SIGNATURE'];

    private static $allowed_additional_fields = [
        'Callback'      => 'CALLBACK',
        'ErrorCallback' => 'ERROR_CALLBACK',
    ];

    /** @var Ecommerce  */
    private $_eCom;

    /**
     * EcommerceFORM constructor.
     * @param Ecommerce $eCom
     */
    public function __construct(Ecommerce $eCom)
    {
        $this->_eCom = $eCom;
    }

    /**
     * Returns array which should be POST'ed to FORMs URL
     *
     * @param $data
     * @param $additional
     * @return array
     * @throws \Exception
     */
    public function getRequest($data, $additional)
    {
        $request = $this->_eCom->prepare($data);

        /* Copy additional fields to request */
        foreach (array_keys(self::$allowed_additional_fields) as $key) {
            if (array_key_exists($key, $additional)) {
                $realKey = self::$allowed_additional_fields[$key];
                $request->$realKey = $additional[$key];
            }
        }

        return (array) $request;

    }

    /**
     * Takes POST array as an argument
     *
     * @param $response
     * @return \SimpleXMLElement
     * @throws \Exception
     */
    public function getResponse($response)
    {
        if (count(array_diff(self::$required, array_keys($response))) > 0) {
            throw new \Exception('Some of required POST params are not included!');
        }

        // Convert array to object
        $responseObj = new \stdClass();
        foreach ($response as $key => $value) {
            $responseObj->$key = $value;
        }

        return $this->_eCom->parse($responseObj);
    }
}