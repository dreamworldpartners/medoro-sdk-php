<?php

namespace DWP\Medoro;

ini_set("soap.wsdl_cache_enabled", "0");

class EcommerceSoap
{
    /** @var Ecommerce  */
    private $_eCom;
    private $_soap;

    /**
     * EcommerceSOAP constructor.
     * @param Ecommerce $eCom
     */
    public function __construct(Ecommerce $eCom)
    {
        $this->_eCom = $eCom;
        $this->_soap = new \SoapClient($eCom->config['wsdl'], array('exceptions' => true));
    }

    /**
     * @param $method
     * @param $arguments
     * @return \SimpleXMLElement
     * @throws \Exception
     */
    public function __call($method, $arguments)
    {
        $request  = $this->_eCom->prepare($arguments[0]);
        $response = $this->_soap->{$method}($request);
        return $this->_eCom->parse($response);
    }
}